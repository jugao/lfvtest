LFV Selection Package Usage
======

Description
-------
To be improved for the LFV search.
Version AnalysisBase/21.2.53

Set up
----
* ssh lxplus
* setupATLAS
* donwload the package
* go to the 'build' directory and setup the AnalysisBase release
* run "cmake ../source' and 'make"
* run "source x86_64-*/setup.sh"

Runing
-----
* In the `source` directiory, there are two python scripts(`testGrid.py` and `testLFV.py`) to run the sample with the sample list in the `run` directory.
* Use "./testGrid.py --help" to see the options
```
Usage: testGrid.py [options]

Options:
  -h, --help            show this help message and exit
  -s SUBMISSION_DIR, --submission-dir=SUBMISSION_DIR
                        submission directory for EventLoop
  -d IS_SIMULATION, --isMC=IS_SIMULATION
                        is data or MC
  -f FILE_LIST, --file-list=FILE_LIST
                        name of file list
  -z NEW_DERIV, --DerivationVesion=NEW_DERIV
                        is New or Old Derovation
  -o OUTPUT_TAG, --output-tag=OUTPUT_TAG
                        tag for grid jobs
```
* For example, to run the test MC sample in the local, go to the `run` dirctory and then run
```
 ./../source/testLFV.py -s JobName -d 1 -f test.list
```
* Run the Data17 sample on the Grid
```
 ./../source/testGrid.py -s JobName -d 0 -f DataList17.txt -o Tag
```

